<?php

namespace Tests\Feature;

use App\Website;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class WebsiteTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    /** init test */
    public function setUp(): void
    {
        parent::setUp();

        // clear
        Website::query()->delete();
    }

    /** @test */
    public function fetchAll()
    {
        $prefix = '[WebsiteFetchAll]';
        $this->print("{$prefix} init");

        $this->print("- creating 3 Websites...");
        factory(Website::class, 3)->create();

        $this->print("- get route");
        $response = $this->getJson(route('websites.index'));

        $this->print("- checking:valid");
        $response->assertStatus(200, 'expectation:statusCode: 200');
        $this->assertCount(3, $response->decodeResponseJson()['data'], 'expectation:response: count(3)');

        $this->print("{$prefix} finish");
    }

    /** @test */
    public function fetchId()
    {
        $prefix = '[WebsiteFetchId]';
        $this->print("{$prefix} init");

        $this->print("- creating 1 Website...");
        $website = factory(Website::class)->create();

        $this->print("- get route");
        $response = $this->getJson(route('websites.show', $website));

        $this->print("- checking:valid");
        $response->assertStatus(200, 'expectation:statusCode: 200');
        $response->assertJsonStructure([
            'message',
            'data' => [
                'id',
                'domain',
                'created_at',
            ],
        ]);

        $this->print("{$prefix} finish");
    }

    /** @test */
    public function create()
    {
        $prefix = '[WebsiteCreate]';
        $this->print("{$prefix} init");

        $this->print("- creating 1 Website:form...");
        $website = factory(Website::class)->make()->getAttributes();

        $this->print("- post route");
        $response = $this->postJson(route('websites.store'), $website);

        $this->print("- checking:valid");
        $response->assertStatus(201, 'expectation:statusCode: 201');
        $this->assertDatabaseHas('websites', [
            'id' => $response->decodeResponseJson()['data']['id'],
        ]);

        $this->print("{$prefix} finish");
    }

    /** @test */
    public function update()
    {
        $prefix = '[WebsiteUpdate]';
        $this->print("{$prefix} init");

        $this->print("- creating 1 Website...");
        $website = factory(Website::class)->create();

        $this->print("- put route");
        $payload = ['domain' => $this->faker->domainName];
        $response = $this->putJson(route('websites.update', $website), $payload);

        $this->print("- checking:valid");
        $response->assertStatus(200, 'expectation:statusCode: 200');
        $this->assertContains($payload['domain'], $response->decodeResponseJson()['data']);

        $this->print("- checking:invalid:validation");
        $payload = ['domain' => null];
        $response = $this->putJson(route('websites.update', $website), $payload);
        $response->assertStatus(422, 'expectation:statusCode: 422');
        $response->assertJsonValidationErrors(['domain']);

        $this->print("{$prefix} finish");
    }

    /** @test */
    public function destroy()
    {
        $prefix = '[WebsiteDestroy]';
        $this->print("{$prefix} init");

        $this->print("- creating 1 Website...");
        $website = factory(Website::class)->create();

        $this->print("- delete route");
        $response = $this->deleteJson(route('websites.destroy', $website));

        $this->print("- checking:valid");
        $response->assertStatus(200, 'expectation:statusCode: 200');

        $this->print("- checking:invalid");
        $response = $this->deleteJson(route('websites.destroy', $website));
        $response->assertStatus(404, 'expectation:statusCode: 404');

        $this->print("{$prefix} finish");
    }
}
