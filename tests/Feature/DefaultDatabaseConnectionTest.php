<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DefaultDatabaseConnectionTest extends TestCase
{
    /** @test */
    public function EnvDatabaseConfigurationIsMysql()
    {
        $prefix = '[EnvDatabaseConfigurationIsMysql]';
        $this->print("{$prefix} init");

        $this->assertSame('test-mysql', getenv('DB_CONNECTION'));
        $this->assertSame('testing', getenv('MYSQL_DATABASE_TEST'));

        $this->print("{$prefix} finish");
    }
}
