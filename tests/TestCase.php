<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Log;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * print and log message
     */
    function print($message): void {
        Log::info($message);

        $content = PHP_EOL . print_r($message, true) . PHP_EOL;
        fwrite(STDERR, $content);
    }

}
