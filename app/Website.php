<?php

namespace App;

use App\Http\Traits\MetaTrait;
use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    use MetaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['domain'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];
}
