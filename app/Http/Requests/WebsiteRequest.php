<?php

namespace App\Http\Requests;

use App\Rules\FQDN;
use Illuminate\Foundation\Http\FormRequest;

class WebsiteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'domain' => ['required', new FQDN()],
        ];
    }
}
