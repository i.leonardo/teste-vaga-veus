<?php

namespace App\Http\Controllers;

use App\Meta;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \App\Meta $meta
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Meta $meta, Request $request)
    {
        return response()->json([
            'message' => 'Retrieved successfully',
            'data' => $meta,
        ]);
    }
}
