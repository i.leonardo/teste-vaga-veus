<?php

namespace App\Http\Controllers;

use App\Http\Requests\WebsiteRequest;
use App\Http\Resources\WebsiteResource;
use App\Website;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    /**
     * GET - fetch_all
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // query
        $website = Website::all();

        return response()->json([
            'message' => 'Retrieved successfully',
            'data' => WebsiteResource::collection($website),
        ]);
    }

    /**
     * POST - create
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebsiteRequest $request)
    {
        // query
        $website = Website::create($request->validated());
        $website->addMeta('website', $website);

        return response()->json([
            'message' => 'Created successfully',
            'data' => new WebsiteResource($website),
        ], 201);
    }

    /**
     * GET - fetch:id
     *
     * @param \App\Website $website
     * @return \Illuminate\Http\Response
     */
    public function show(Website $website)
    {
        return response()->json([
            'message' => 'Retrieved successfully',
            'data' => new WebsiteResource($website),
        ]);
    }

    /**
     * PUT|PATCH - update:id
     *
     * @param \App\Website $website
     * @return \Illuminate\Http\Response
     */
    public function update(WebsiteRequest $request, Website $website)
    {
        // query
        $website->update($request->validated());
        $website->updateMeta('website', $website);

        return response()->json([
            'message' => 'Update successfully',
            'data' => new WebsiteResource($website),
        ]);
    }

    /**
     * DELETE - destroy:id
     *
     * @param \App\Website $website
     * @return \Illuminate\Http\Response
     */
    public function destroy(Website $website)
    {
        // query
        $website->delete();
        $website->deleteMeta('website', $website);

        return response()->json([
            'message' => 'Deleted successfully',
        ]);
    }
}
